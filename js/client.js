/*
  Handles the drop event and fileinput event.
  Start app and create fileobject which is passed to mosaic app.
*/

var dropDiv = document.querySelector('.box-input');
var uploadBtn = document.getElementById('upload');
var fileInput = document.getElementById('fileinput');
var svgurl = 'http://localhost:8765/color/';

/*
  Custom logger
  Some helpers
*/

function _log() {
  if (console) {
    console.log.apply(console, arguments);
  }
}

var helpers = {

  /*
    Returns base64 svg.
  */
  svg: function(resp) {
    return 'data:image/svg+xml;base64,' + btoa(resp);
  },

  /*
  Simple get request.
  */
  httpget: function(url) {
    return new Promise(function(resolve, reject) {
      var xmlHttp = new XMLHttpRequest();
      xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
          resolve(xmlHttp.responseText);
        }
      }
      xmlHttp.open("GET", url);
      xmlHttp.send(null);
    });
  },

  /*
    Stop events : Somewhat vulnerable  
  */

  stopEvents: function(e) {
    e.preventDefault();
    e.stopPropagation();
  },

  rgbToHex: function (r, g, b) {
    var rgb = b | (g << 8) | (r << 16);
    return (0x1000000 + rgb).toString(16).slice(1)
  },

  /*
    Clear canvas for redrawing.
  */

  clearCanvas: function(canvas) {
    for (var i of canvas) {
      var canvas = document.getElementById(i);
      var context = canvas.getContext('2d');
      context.fillStyle = "#ffffff";
      context.fillRect(0, 0, canvas.width, canvas.height);
    }
  }
}


/*
  Handle drag/drop/fileinput event and expose file as object.
*/

function getFile(e) {
  return new Promise(function(resolve, reject) {
    // check if file is from input or drop event and get first file
    if ('dataTransfer' in e && e.dataTransfer.files.length) {
      var fileObject = e.dataTransfer.files[0];
    } else {
      var fileObject = fileinput.files[0];
    }

    // Do not allow file which doesn't match content type of image.
    if (!fileObject.type.match('image.*')) {
      reject('File is not an image.');
      return;
    }

    resolve(fileObject);
  });
}


/*
  Read file as text using fileobject
*/

function handleFile(fileObject) {
  return new Promise(function(resolve, reject) {
    var reader = new FileReader();
    reader.onload = function(e) {
      resolve(e.target);
    }
    reader.onerror = function(e) {
      reject(e);
    }
    reader.readAsDataURL(fileObject);
  });
}


/*
  Uses file and create image.
*/

function createImage(file) {
  return new Promise(function(resolve, reject) {
    var image = new Image();
    image.src = file.result;
    image.onload = function() {
      resolve(image);
    };
  });
}


/*
  Returns imagedata (context, imagewidth, imageheight) using in memory canvas .
*/

function getImageData(image) {
  return new Promise(function(resolve, reject) {
    var canvas = document.createElement('canvas');
    canvas.width = image.width;
    canvas.height = image.height;
    var context = canvas.getContext('2d');
    context.drawImage(image, 0, 0);

    var imageData = {
      ctx: context,
      width: image.width,
      height: image.height
    };
    resolve(imageData);
  });
}


/*
  Get tile average color
*/

function tileAverage(tile) {
  var pixels = tile.data.length;
  var count = 0;
  var rgb = {
    r: 0,
    g: 0,
    b: 0
  };

  for(var i = 0; i < pixels; i += 4){
    rgb.r += tile.data[i] || 0;
    rgb.g += tile.data[i+1] || 0;
    rgb.b += tile.data[i+2] || 0;
    count ++;
  }
  rgb.r = ~~(rgb.r/count);
  rgb.g = ~~(rgb.g/count);
  rgb.b = ~~(rgb.b/count);
  
  return rgb;
}


/*
  Compute tiles average color and positions.
  also computes rows values and returns it.
*/

function computeImage(ctx, imageWidth, imageHeight) {
  return new Promise(function(resolve, reject) {
    var tiles = [];
    var rows = {};
    var tileWidth = TILE_WIDTH;
    var tileHeight = TILE_HEIGHT;

    for(var w = 0; w < imageWidth; w += tileWidth){
      for(var h = 0; h < imageHeight; h += tileHeight){
        var tile = ctx.getImageData(w, h, tileWidth, tileHeight);
        var tilecolor = tileAverage(tile);
        tilecolor.x = w; tilecolor.y = h;
        tilecolor.hex = helpers.rgbToHex(tilecolor.r, tilecolor.g, tilecolor.b);
        tiles.push(tilecolor);

        // This is further used to draw svg row by row
        if (h in rows) {
          rows[h].push(tilecolor);
        } else {
          rows[h] = [tilecolor];
        }
      }
    }
    resolve([tiles, rows]);
  });
}


/*
  Fill canvas with rect/circle shape. Iteratively.
*/

function canvasMosaic(tiles, canvasid, shape) {
  var canvas = document.getElementById(canvasid);
  canvas.width = tiles[tiles.length - 1].x;
  canvas.height = tiles[tiles.length - 1].y;
  var context = canvas.getContext('2d');

  if (shape == 'circle') {
    for (var i = 0; i < tiles.length; i++) {
      context.beginPath();
      context.arc(tiles[i].x, tiles[i].y, TILE_WIDTH/2, 0, 2 * Math.PI);
      context.fillStyle = '#' + tiles[i].hex;
      context.fill();
    }
  } else if (shape == 'rect') {
    for (var i = 0; i < tiles.length; i++) {
      context.fillStyle = '#' + tiles[i].hex;
      context.fillRect(tiles[i].x, tiles[i].y, TILE_WIDTH, TILE_HEIGHT);
    }
  }
}


/*
  Drawrow called recrusively until no rows left.
*/

function drawRow(rows, rowIndex, context) {
  return new Promise(function(resolve, reject) {
    var requests = [];
    rows[rowIndex].forEach(function(pixel) {
      requests.push(helpers.httpget(svgurl + pixel.hex));
    });
    Promise.all(requests)
    .then(function(responses) {
      var columnIndex = 0;
      responses.forEach(function(resp) {
        var img = new Image();
        img.onload = function() {
          context.drawImage(img, columnIndex, rowIndex);
          columnIndex += TILE_WIDTH;
        }
        img.src = helpers.svg(resp);
      });
      rowIndex += TILE_WIDTH;
      if (rows[rowIndex]) {
        return drawRow(rows, rowIndex, context);
      }
    });
  });
}


/*
  SVG Drawing from top to bottom row by row.
*/

function svgDraw(tiles, rows, imageData, canvasid) {
  var canvas = document.getElementById(canvasid);
  canvas.width = imageData.width;
  canvas.height = imageData.height;
  var context = canvas.getContext('2d');
  drawRow(rows, 0, context);
  // do anything here. This isn't fast and can be optimized using
  // better canvas function and request sending techniques.
  // No need to send requests to fetch svg instead draw on canvas for performance.
}

/*
  Main function called after receiving upload or drop event.
  Its also the starting of the app.
*/

function main(e) {
  helpers.stopEvents(e);
  if (e.type === 'dragover') { return; } //handle drag event, doesn't do much
  var imageGlobal;

  getFile(e)
  .then(function(fileObject) {
    helpers.clearCanvas(['circleoutput', 'rectoutput']);
    return handleFile(fileObject);
  })
  .then(function(file) {
    return createImage(file);
  })
  .then(function(image) {
    return getImageData(image);
  })
  .then(function(imageData) {
    imageGlobal = imageData;
    return computeImage(imageData.ctx, imageData.width, imageData.height);
  })
  .then(function([tiles, rows]) {
    canvasMosaic(tiles, 'circleoutput', 'circle');
    canvasMosaic(tiles, 'rectoutput', 'rect');
    svgDraw(tiles, rows, imageGlobal, 'svgoutput');
  })
  .catch(function(e) {
    _log(e);
  });
}


/*
  Atart listening for events but should be used with modernizr etc.
*/

dropDiv.addEventListener('dragover', main, false);
dropDiv.addEventListener('drop', main, false);
uploadBtn.addEventListener('click', main);
